const { getMessagesData, saveMessagesData } = require('../repositories/message.repository');

const getMessages = () => {
  const content = getMessagesData();

  if (content) {
    const messages = Array.from(JSON.parse(content));

    return messages;
  } else {
    return null;
  }
}

const getMessageById = id => {
  if (id) {
    const messages = getMessages();
    const result = messages.find(message => message.id === id);

    return result;
  } else {
    return null;
  }
}

const saveNewMessage = message => {
  if (message) {
    const messages = getMessages();
    messages.push(message);

    return saveMessagesData(messages);
  } else {
    return false;
  }
}

const putAndSaveMessage = (id, message) => {
  if (id && message) {
    const messages = getMessages();
    const index = messages.findIndex(message => message.id === id);
    messages[index] = message;

    return saveMessagesData(messages);
  } else {
    return false;
  }
}

const deleteMessageById = id => {
  if (id) {
    const messages = getMessages();
    const newMessages = messages.filter(message => message.id !== id);

    return saveMessagesData(newMessages);
  } else {
    return false;
  }
}

const addLikeToMessage = (id, username) => {
  if(id && username) {
    const messages = getMessages();
    const index = messages.findIndex(message => message.id === id);
    const modifiedMessage = { ...messages[index] }
    modifiedMessage.likes.push(username);
    messages[index] = modifiedMessage;

    console.log(messages);
    return saveMessagesData(messages);
  } else {
    return false;
  }
};

const deleteLikeFromMessage = (id, username) => {
  if(id && username) {
    const messages = getMessages();
    const index = messages.findIndex(message => message.id === id);
    const modifiedMessage = { ...messages[index] };
    modifiedMessage.likes = modifiedMessage.likes.filter(name => name !== username);
    messages[index] = modifiedMessage;

    return saveMessagesData(messages);
  } else {
    return false;
  }
};

module.exports = {
  getMessages,
  getMessageById,
  saveNewMessage,
  putAndSaveMessage,
  deleteMessageById,
  addLikeToMessage,
  deleteLikeFromMessage
}