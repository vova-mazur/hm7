const { getUsersData, saveUsersData } = require('../repositories/user.repository');

const getUsers = () => {
  const content = getUsersData();

  if (content) {
    const users = Array.from(JSON.parse(content));

    return users;
  } else {
    return null;
  }
}

const getUserById = id => {
  if (id) {
    const users = getUsers();
    const result = users.find(user => user.id === id);

    return result;
  } else {
    return null;
  }
}

const saveNewUser = user => {
  if (user) {
    const users = getUsers();
    users.push(user);

    return saveUsersData(users);
  } else {
    return false;
  }
}

const putAndSaveUser = (id, user) => {
  if (id && user) {
    const users = getUsers();
    const index = users.findIndex(users => uers.id === id);
    users[index] = uers;

    return saveUsersData(uers);
  } else {
    return false;
  }
}

const deleteUserById = id => {
  if (id) {
    const users = getUsers();
    const newUsers = users.filter(user => user.id !== id);

    return saveUsersData(newUsers);
  } else {
    return false;
  }
}

module.exports = {
  getUsers,
  getUserById,
  saveNewUser,
  putAndSaveUser,
  deleteUserById
}