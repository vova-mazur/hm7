const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const usersRouter = require('./routes/users');
const messageRouter = require('./routes/messages');
const authRouter = require('./routes/auth');
const cors = require('cors');
const app = express();

require('./passport.config');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize());
app.use(cors());
app.use((req, res, next) => setTimeout(() => next(), 300)); // in order to oversee a loading spinner
app.use('/users', usersRouter);
app.use('/messages', messageRouter);
app.use('/login', authRouter);

app.listen(5000);