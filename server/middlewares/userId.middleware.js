const { getUsers } = require('../services/user.service')

const validUserId = (req, res, next) => {
  if (
		req &&
		req.params &&
		req.params.id
	) {
		const { params: { id } } = req;
		const reg = /^\d+$/;
		if (reg.test(id)) {
			const users = getUsers();
			if(users.filter(user => user.id === id).length > 0) {
				return next();
			}
		}
	}
	res.status(404).end('invalid user id');
}

module.exports = {
  validUserId
}