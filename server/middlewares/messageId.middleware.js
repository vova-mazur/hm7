const { getMessages } = require('../services/message.service')

const validMessageId = (req, res, next) => {
  if (
		req &&
		req.params &&
		req.params.id
	) {
		const { params: { id } } = req;
		const reg = /^\d+$/;
		if (reg.test(id)) {
			const messages = getMessages();
			if(messages.filter(message => message.id === id).length > 0) {
				return next();
			}
		}
	}
	res.status(404).end('invalid message id');
}

module.exports = {
  validMessageId
}