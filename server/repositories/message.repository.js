const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, '/../data', 'messages.json');

const getMessagesData = () => {
  const fileContent = fs.readFileSync(filePath);

  if(fileContent) {
    return fileContent
  } else {
    return null;
  }
};

const saveMessagesData = data => {
  if(data) {
    const content = JSON.stringify(data, null, 2);
    fs.writeFileSync(filePath, content);
    return true;
  } else {
    return false;
  }
};

module.exports = {
  getMessagesData,
  saveMessagesData
}