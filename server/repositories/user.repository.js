const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, '/../data', 'users.json');

const getUsersData = () => {
  const fileContent = fs.readFileSync(filePath);

  if(fileContent) {
    return fileContent
  } else {
    return null;
  }
};

const saveUsersData = data => {
  if(data) {
    const content = JSON.stringify(data, null, 2);
    fs.writeFileSync(filePath, content);
    return true;
  } else {
    return false;
  }
};

module.exports = {
  getUsersData,
  saveUsersData
}