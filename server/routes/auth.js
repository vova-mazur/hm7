const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const users = require('../data/users.json');

router.post('/', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'secret', { expiresIn: '24h' });
    res.status(200).json({
      auth: true,
      token,
      name: userInDB.name,
      avatar: userInDB.avatar,
      role: userInDB.name === 'Admin' ? 'admin' : 'user'
    });
  } else {
    res.status(401).json({ auth: false });
  }
});

module.exports = router;