const express = require('express');
const router = express.Router();

const { validUserId } = require('../middlewares/userId.middleware');
const {
  getUsers,
  getUserById,
  saveNewUser,
  putAndSaveUser,
  deleteUserById
} = require('../services/user.service');

router.use('/:id', validUserId);

router.get('/', (req, res) => {
  const users = getUsers();

  if (users) {
    res.status(200).json(users);
  } else {
    res.status(404).send('Data error');
  }
});

router.get('/:id', (req, res) => {
  const { params: { id } } = req;
  const user = getUserById(id);

  if (user) {
    res.status(200).json(user);
  } else {
    res.status(404).send('Data error');
  }
});

router.post('/', (req, res) => {
  const result = saveNewUser(req.body);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.put('/:id', (req, res) => {
  const { params: { id } } = req;
  const result = putAndSaveUser(id, req.body);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.delete('/:id', (req, res) => {
  const { params: { id } } = req;
  const result = deleteUserById(id);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

module.exports = router;