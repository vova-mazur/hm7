const express = require('express');
const router = express.Router();

const { validMessageId } = require('./../middlewares/messageId.middleware');
const {
  getMessages,
  getMessageById,
  saveNewMessage,
  putAndSaveMessage,
  deleteMessageById,
  addLikeToMessage,
  deleteLikeFromMessage
} = require('../services/message.service');

router.use('*/:id', validMessageId);

router.get('/', (req, res) => {
  const messages = getMessages();

  if (messages) {
    res.status(200).json(messages);
  } else {
    res.status(404).send('Data error');
  }
});

router.get('/:id', (req, res) => {
  const { params: { id } } = req;
  const message = getMessageById(id);

  if (message) {
    res.status(200).json(message);
  } else {
    res.status(404).send('Data error');
  }
});

router.post('/', (req, res) => {
  const result = saveNewMessage(req.body);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.put('/:id', (req, res) => {
  const { params: { id } } = req;
  const result = putAndSaveMessage(id, req.body);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.delete('/:id', (req, res) => {
  const { params: { id } } = req;
  const result = deleteMessageById(id);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.post('/likes/:id', (req, res) => {
  const { 
    params: { id }, 
    body: { name } 
  } = req;

  const result = addLikeToMessage(id, name);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

router.delete('/likes/:id', (req, res) => {
  const { 
    params: { id }, 
    body: { name } 
  } = req;

  const result = deleteLikeFromMessage(id, name);

  if (result) {
    res.status(200).send({ success: 'success' });
  } else {
    res.status(500).send('Database error');
  }
});

module.exports = router;