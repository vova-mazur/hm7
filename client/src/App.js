import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';

import LoginPage from './login';
import Chat from './chat/index';
import MessageEditor from './messageEditor';
import UserList from './userList/';
import UserPage from './userPage';
import Loading from './loading';

const App = () => (
  <div className="App">
    <Loading />
    <Switch>
      <Route exact path="/" component={LoginPage} />
      <Route path="/chat" component={Chat} />
      <Route path="/editor" component={MessageEditor} />
      <Route path="/users" componennt={UserList} />
      <Route path="/user" component={UserPage} />
      <Route path="/user/:id" component={UserPage} />
    </Switch>
  </div>
)

export default App;
