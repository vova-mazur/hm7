import { SET_LOADING, CANCEL_LOADING } from './actionTypes';

export default function loadingReducer(state = false, action) {
  if (action.type === SET_LOADING) {
    return true;
  } else if(action.type === CANCEL_LOADING) {
    return false;
  } else {
    return state;
  }
};