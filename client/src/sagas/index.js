import { all } from 'redux-saga/effects';
import messagesSagas from '../chat/sagas';
import loginSagas from '../login/sagas';
import userPageSagas from '../userPage/sagas';
import usersSagas from '../userList/sagas';

export default function* rootSaga() {
  yield all([
    messagesSagas(),
    loginSagas(),
    userPageSagas(),
    usersSagas(),   
  ])
}