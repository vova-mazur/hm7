import { LOGIN_USER, SET_CURRENT_USER } from './actionTypes';

export const loginUser = user => ({
  type: LOGIN_USER,
  payload: {
    user
  }
});

export const setCurrentUser = ({ jwtToken, name, avatar }) => ({
  type: SET_CURRENT_USER,
  payload: {
    jwtToken,
    name,
    avatar,
  }
});