import axios from 'axios';
import api from './../shared/config/api';
import { put, call, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_ERROR } from './actionTypes';

import { setLoading, cancelLoading } from './../loading/actions';

export function* loginUser(action) {
  const { email, password } = action.payload.user;
  try {
    yield put(setLoading());
    const res = yield call(axios.post, `${api.url}/login`, { login: email, password });
    const { auth, token, name, role, avatar } = res.data;
    if(auth) {
      localStorage.setItem('jwtToken', token);
      localStorage.setItem('name', name);
      localStorage.setItem('role', role);
      localStorage.setItem('avatar', avatar);
      yield put({ type: LOGIN_USER_SUCCESS, payload: { token, username: name, avatar, role } });
    } else {
      yield put({ type: LOGIN_USER_ERROR, payload: { error: 'Unauthorized'} });
    }
    yield put(cancelLoading());
  } catch(error) {
    yield put({ type: LOGIN_USER_ERROR, payload: { error } });
  }
}

function* watchUserAuthentication() {
  yield takeEvery(LOGIN_USER, loginUser);
}

export default function* loginSaga() {
  yield all([
    watchUserAuthentication()
  ]);
}