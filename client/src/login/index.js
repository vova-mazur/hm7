import React from 'react';
import useForm from './userForm';
import validate from './LoginFormValidationRules';
import { loginUser } from './actions';
import { connect } from 'react-redux';
// import browserHistory from './../history';

const LoginPage = ({ loginUser, isAuthenticated, error, role, history }) => {
  if (isAuthenticated) {
    if (role === 'admin') {
      history.push('/users');
    } else {
      history.push('/chat');
    }
  }

  const login = () => {
    loginUser(values);
  }

  const {
    values,
    errors,
    handleChange,
    handleSubmit
  } = useForm(login, validate);

  return isAuthenticated ? null : (
    <div className="section is-fullheight">
      <div className="container">
        <div className="column is-4 is-offset-4">
          <div className="box">
            <form onSubmit={handleSubmit} noValidate>
              <div className="field">
                <label className="label">Email Address</label>
                <div className="control">
                  <input
                    autoComplete="off"
                    className={`input ${errors.email && 'is-danger'}`}
                    type="email"
                    name="email"
                    onChange={handleChange}
                    value={values.email || ''}
                    required
                  />
                  {errors.email && (
                    <p className="help is-danger">{errors.email}</p>
                  )}
                </div>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <div className="control">
                  <input
                    className={`input ${errors.password && 'is-danger'}`}
                    type="password"
                    name="password"
                    onChange={handleChange}
                    value={values.password || ''}
                    required
                  />
                </div>
                {errors.password && (
                  <p className="help is-danger">{errors.password}</p>
                )}
              </div>
              <button type="submit" className="button is-block is-info is-fullwidth">Login</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  isAuthenticated: state.login.isAuthenticated,
  error: state.login.error,
  role: state.login.role,
});

export default connect(mapStateToProps, { loginUser })(LoginPage);