import { LOGIN_USER_SUCCESS, LOGIN_USER_ERROR, SET_CURRENT_USER } from './actionTypes';

const initialState = {
  isAuthenticated: false,
  username: undefined,
  avatar: undefined,
  token: undefined,
  error: undefined,
}

export default function(state = initialState, action) {
  switch(action.type) {
    case LOGIN_USER_SUCCESS: {
      const { token, username, avatar } = action.payload; 
      return { 
        ...state,
        isAuthenticated: true,
        username: username,
        avatar: avatar,
        token: token,
      };
    }
    case LOGIN_USER_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        error,
      }
    }
    case SET_CURRENT_USER: {
      const { jwtToken, name, avatar } = action.payload;

      console.log('set');
      return {
        ...state,
        isAuthenticated: true,
        username: name,
        avatar: avatar,
        token: jwtToken,
      }
    }
    default:
      return state;
  }
};