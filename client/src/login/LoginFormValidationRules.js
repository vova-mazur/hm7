export default function validate(values) {
  const { email, password } = values;

  if(email === 'admin' && password === 'admin') {
    return {};
  }
  let errors = {};

  if (!email) {
    errors.email = 'Email address is required';
  } else if (!/\S+@\S+\.\S+/.test(email)) {
    errors.email = 'Email address is invalid';
  }
  
  if (!password) {
    errors.password = 'Password is required';
  } else if (password.length < 8) {
    errors.password = 'Password must be 8 or more characters';
  }
  return errors;
};