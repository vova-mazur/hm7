import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import { setCurrentMessage } from './../messageEditor/actions';
import browserHistory from './../history';

import Header from './components/Header';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ''
    }

    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onAddLike = this.onAddLike.bind(this);
    this.onDeleteLike = this.onDeleteLike.bind(this);
    this.onSend = this.onSend.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.fetchMessages();
  }

  onEdit(message) {
    this.props.setCurrentMessage(message);
    // this.props.showPage();
    // this.props.history.push('/editor');
    // browserHistory.push('/editor');
    console.log('edit');
  }

  onDelete(id) {
    if (window.confirm('Delete this message ?')) {
      this.props.deleteMessage(id);
    }
  }

  onAddLike(id) {
    this.props.addLikeToMessage(id, this.props.username);
  }

  onDeleteLike(id) {
    this.props.deleteLikeFromMessage(id, this.props.username);
  }

  onChange(e) {
    this.setState({
      message: e.target.value,
    })
  }

  onSend() {
    const { message } = this.state;
    if (message !== '') {
      const { username, avatar } = this.props;
      const date = {
        user: username,
        avatar: avatar,
        created_at: new Date().toLocaleString(),
        message,
        marked_read: false,
        likes: []
      }
      this.props.addMessage(date);
      this.setState({
        message: ''
      })
    }
  }

  getCountOfParticipantsInChat = () => {
    const participants = [];
    this.props.messages.forEach(message => {
      const { user } = message;
      if (!participants.includes(user)) {
        participants.push(user);
      }
    });
    return participants.length;
  }

  render() {
    return (
      <div className="Chat">
        {
          this.props.loading || this.props.messages.length === 0 ? null :
            <div>
              <Header
                messages={this.props.messages}
                countOfParticipants={this.getCountOfParticipantsInChat()}
              />
              <MessageList
                onEdit={this.onEdit}
                onDelete={this.onDelete}
                onAddLike={this.onAddLike}
                onDeleteLike={this.onDeleteLike}
                messages={this.props.messages}
                currentUser={this.props.username}
              />
              <MessageInput
                onSend={this.onSend}
                onChange={this.onChange}
                message={this.state.message}
              />
            </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  username: state.login.username,
  avatar: state.login.avatar,
  loading: state.loading,
});

const mapDispatchToProps = {
  ...actions,
  setCurrentMessage,
  // showPage,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);