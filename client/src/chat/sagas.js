import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE,
  ADD_LIKE, DELETE_LIKE
} from './actionTypes';

import { setLoading, cancelLoading } from './../loading/actions';

const MESSAGES_URL = `${api.url}/messages`;

export function* fetchMessages() {
  try {   
    yield put(setLoading()); 
    const messages = yield call(axios.get, MESSAGES_URL);
    yield put({ type: 'FETCH_MESSAGES_SUCCESS', payload: { messages: messages.data } });   
  } catch (error) {
    console.log('fetchMessages error:', error.message)
  } finally {
    yield put(cancelLoading()); 
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
  const { data, id } = action.payload;
  const newMessage = { id, ...data };

  try {
    yield put(setLoading()); 
    yield call(axios.post, MESSAGES_URL, newMessage);
    yield put({ type: FETCH_MESSAGES });  
  } catch (error) {
    console.log('createMessage error:', error.message);
  } finally {
    yield put(cancelLoading());
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* updateMessage(action) {
  const { id, data } = action.payload;
  const updatedMessage = { ...data };

  try {
    yield put(setLoading()); 
    yield call(axios.put, `${MESSAGES_URL}/${id}`, updatedMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log('updateMessafe error:', error.message);
  } finally {
    yield put(cancelLoading()); 
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
  try {
    yield put(setLoading());
    yield call(axios.delete, `${MESSAGES_URL}/${action.payload.id}`);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log('deleteMessage error:', error.message);
  } finally {
    yield put(cancelLoading()); 
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* addLikeToMessage(action) {
  const { id, name } = action.payload;

  try {
    yield put(setLoading());
    yield call(axios.post, `${MESSAGES_URL}/likes/${id}`, { name });
    yield put({ type: FETCH_MESSAGES });
  } catch(error) {
    console.log('addLikeError ', error.message);
  } finally {
    yield put(cancelLoading()); 
  }
}

function* watchAddLikeToMessage() {
  yield takeEvery(ADD_LIKE, addLikeToMessage);
}

export function* deleteLikeFromMessage(action) {
  const { id, name } = action.payload;

  try {
    yield put(setLoading());
    yield call(axios.delete, `${MESSAGES_URL}/likes/${id}`, { data: { name } });
    yield put({ type: FETCH_MESSAGES });
  } catch(error) {
    console.error('deleteLikeError ', error.message);
  } finally {
    yield put(cancelLoading()); 
  }
}

function* watchDeleteLikeFromMessage() {
  yield takeEvery(DELETE_LIKE, deleteLikeFromMessage);
}

export default function* messagesSagas() {
  yield all([
    watchFetchMessages(),
    watchAddMessage(),
    watchUpdateMessage(),
    watchDeleteMessage(),
    watchAddLikeToMessage(),
    watchDeleteLikeFromMessage(),
  ])
};
