import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';

const Header = ({ messages, countOfParticipants }) => (
  <header className="my-header">
    <div className="meta">
      <span>My chat</span>
      <span>{countOfParticipants} participants</span>
      <span>{messages.length} messages</span>
    </div>
    <div className="my-time-container">
      last message at
      <span className="time">
        {messages[messages.length - 1].created_at.split(' ')[1]}
      </span>
    </div>
  </header>
)

Header.propTypes = {
  messages: PropTypes.array,
  countOfParticipants: PropTypes.number
}

export default Header;
