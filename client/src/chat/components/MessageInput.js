import React from 'react';
import PropTypes from 'prop-types';
import './MessageInput.css';

const MessageInput = ({ onChange, onSend, message }) => (
  <div className="message-input">
    <input
      type="text"
      placeholder="Type a message"
      onChange={onChange}
      value={message}
    />
    <button onClick={onSend}>Send</button>
  </div>
);

MessageInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSend: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired
}

export default MessageInput;