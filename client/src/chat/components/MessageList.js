import React from 'react';
import PropTypes from 'prop-types';
import MessageItem from './MessageItem';
import './MessageList.css';

const MessageList = ({ onEdit, onDelete, onAddLike, onDeleteLike, messages, currentUser }) => {
  const createMessages = () => {
    let currentDate = messages[0].created_at.split(' ')[0];

    return messages.map(messageInfo => {
      const { id, created_at } = messageInfo;

      let date = created_at.split(' ')[0];
      const addSpliter = currentDate !== date || messages.indexOf(messageInfo) === 0;
      currentDate = date;

      return (
        <div key={id}>
          {addSpliter ? <div className="spliter">{created_at.split(' ')[0]}</div> : null}
          <MessageItem
            currentUser={currentUser}
            messageInfo={messageInfo}
            onEdit={onEdit}
            onDelete={onDelete}
            onAddLike={onAddLike}
            onDeleteLike={onDeleteLike}
            canEdit={messages.indexOf(messageInfo) === messages.length - 1}
          />
        </div>
      );
    })
  }

  return (
    <div className="messages">
      {createMessages()}
    </div>
  );
}

MessageList.propTypes = {
  onEdit: PropTypes.func.isRequired, 
  onDelete: PropTypes.func.isRequired, 
  onAddLike: PropTypes.func.isRequired, 
  onDeleteLike: PropTypes.func.isRequired, 
  messages: PropTypes.array.isRequired, 
  currentUser: PropTypes.string.isRequired
}

export default MessageList;