import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const MessageItem = ({ messageInfo, currentUser, canEdit, onEdit, onDelete, onDeleteLike, onAddLike }) => {
  const handleKeyPress = event => {
    if (event.keyCode === 38) {
      onEdit(messageInfo);
    }
  };

  useEffect(() => {
    if (canEdit) {
      document.addEventListener('keydown', handleKeyPress);
      return () => {
        document.removeEventListener('keydown', handleKeyPress);
      }
    }
  });

  const { id, user, avatar, created_at, message, likes } = messageInfo;

  return (
    <div className="my-message">
      {user === currentUser ? null : <img src={avatar} alt="avatar" />}
      <div className="content">
        <div className="title">
          <p className="author">{user}</p>
          <p className="data">{created_at}</p>
        </div>
        <p className="text">{message}</p>
      </div>
      {
        user === currentUser 
          ? <div>
              {canEdit
                ? <button className="hidden" onClick={(e) => onEdit(messageInfo)}>Edit</button>
                : null}
              <button onClick={(e) => onDelete(id)}>Delete</button>
            </div>
          : <div>
              {likes && likes.includes(currentUser)
                ? <button onClick={(e) => onDeleteLike(id)}>Unlike</button> 
                : <button onClick={(e) => onAddLike(id)}>Like</button>
              }
            </div>
      }
    </div>
  )
}

MessageItem.propTypes = {
  currentUser: PropTypes.string.isRequired,
  onDeleteLike: PropTypes.func.isRequired,
  onAddLike: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  canEdit: PropTypes.bool.isRequired,
  messageInfo: PropTypes.object.isRequired
}

export default MessageItem;