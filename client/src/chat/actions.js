import { 
  FETCH_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE,
  ADD_LIKE, DELETE_LIKE 
} from './actionTypes';
import service from '../services/idService';

export const fetchMessages = () => ({
  type: FETCH_MESSAGES
})

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getNewId(),
    data
  }
})

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: { 
    id
   }
})

export const updateMessage = (id, data) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id, 
    data
  }
})

export const addLikeToMessage = (id, name) => ({
  type: ADD_LIKE,
  payload: {
    id,
    name
  }
})

export const deleteLikeFromMessage = (id, name) => ({
  type: DELETE_LIKE,
  payload: {
    id, 
    name
  }
})