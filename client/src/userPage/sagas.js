import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USER, FETCH_USER_SUCCESS } from "./actionTypes";
import { setLoading, cancelLoading } from './../loading/actions';

export function* fetchUser(action) {
  try {
    yield put(setLoading());
    const user = yield call(axios.get, `${api.url}/users/${action.payload.id}`);
    yield put({ type: FETCH_USER_SUCCESS, payload: { userData: user.data } })
  } catch (error) {
    console.log('fetchUsers error:', error.message)
  } finally {
    yield put(cancelLoading());
  }
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER, fetchUser)
}

export default function* userPageSagas() {
  yield all([
    watchFetchUser()
  ])
};
