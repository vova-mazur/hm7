import { combineReducers } from 'redux';

import users from "../userList/reducer";
import userPage from "../userPage/reducer";
import loadingReducer from './../loading/reducer';
import chatReducer from './../chat/reducer';
import modalReducer from './../messageEditor/reducer';
import loginReducer from './../login/reducer';

export default combineReducers({
  loading: loadingReducer,
  messages: chatReducer,
  modal: modalReducer,
  login: loginReducer,
  users,
  userPage
});