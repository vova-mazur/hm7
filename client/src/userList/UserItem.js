import React from 'react';
import PropTypes from 'prop-types';

const UserItem = ({ id, name, surname, email, onEdit, onDelete }) => (
  <div className="container list-group-item">
    <div className="row">
      <div className="col-8">
        <span className="badge badge-secondary float-left" style={{ fontSize: "2em", margin: "2px" }}>{name} {surname}</span>
        <span className="badge badge-info" style={{ fontSize: "2em", margin: "2px" }}>{email}</span>
      </div>
      <div className="col-4 btn-group">
        <button className="btn btn-outline-primary" onClick={(e) => onEdit(id)}> Edit </button>
        <button className="btn btn-outline-dark" onClick={(e) => onDelete(id)}> Delete </button>
      </div>
    </div>
  </div>
);

UserItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  surname: PropTypes.string,
  email: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
}

export default UserItem;
