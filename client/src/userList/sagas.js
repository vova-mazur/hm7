import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS } from './actionTypes';

const USERS_URL = `${api.url}/users`;

export function* fetchUsers() {
  try {
    const users = yield call(axios.get, USERS_URL);
    yield put({ type: 'FETCH_USERS_SUCCESS', payload: { users: users.data } })
  } catch (error) {
    console.log('fetchUsers error:', error.message)
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS, fetchUsers)
}

export function* addUser(action) {
  const { data, id } = action.payload;
  const newUser = { ...data, id };

  try {
    yield call(axios.post, USERS_URL, newUser);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    console.log('createUser error:', error.message);
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser)
}

export function* updateUser(action) {
  const { id, data } = action.payload;
  const updatedUser = { ...data };

  try {
    yield call(axios.put, `${USERS_URL}/${id}`, updatedUser);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    console.log('updateUser error:', error.message);
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER, updateUser)
}

export function* deleteUser(action) {
  try {
    yield call(axios.delete, `${USERS_URL}/${action.payload.id}`);
    yield put({ type: FETCH_USERS })
  } catch (error) {
    console.log('deleteUser Error:', error.message);
  }
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER, deleteUser)
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUser(),
    watchUpdateUser(),
    watchDeleteUser()
  ])
};
