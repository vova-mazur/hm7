import { SET_CURRENT_MESSAGE, DROP_CURRENT_MESSAGE } from './actionTypes';

const intialState = {
  currentMessage: {},
};

export default function (state = intialState, action) {
  switch (action.type) {
    case SET_CURRENT_MESSAGE: {
      const { message } = action.payload;
      return {
        ...state,
        currentMessage: message
      }
    }
    case DROP_CURRENT_MESSAGE: {
      return {
        ...state,
        currentMessage: {}
      }
    }
    default:
      return state;
  }
}