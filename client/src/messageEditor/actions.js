import { SET_CURRENT_MESSAGE, DROP_CURRENT_MESSAGE } from './actionTypes';

export const setCurrentMessage = message => ({
  type: SET_CURRENT_MESSAGE,
  payload: {
    message
  }
});

export const dropCurrentMessage = () => ({
  type: DROP_CURRENT_MESSAGE
});
