import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from './actions';
import { updateMessage } from './../chat/actions';
import browserHistory from './../history';

class MessageEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newMessageContent: '',
    }

    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.currentMessage.message) {
      this.setState({
        newMessageContent: newProps.currentMessage.message
      })
    }
  }

  onCancel() {
    this.props.dropCurrentMessage();
    // this.props.hidePage();
    this.setState({
      newMessageContent: ''
    })
    browserHistory.push('/chat')
    // this.props.history.push('/chat');
  }

  onSave() {
    const { newMessageContent } = this.state;
    if (newMessageContent !== '') {
      const { id } = this.props.currentMessage;
      this.props.updateMessage(id, newMessageContent);
      this.props.dropCurrentMessage();
      // this.props.hidePage();
      this.setState({
        newMessageContent: ''
      });
    } else {
      alert('The message can not be empty!')
    }
    this.props.history.push('/chat');
  }

  onChange(e) {
    this.setState({
      newMessageContent: e.target.value
    })
  }

  // getModalContent() {
  //   console.log('content');
  //   return (
  //     <div className="modal">
  //       <h3>Edit message</h3>
  //       <textarea value={this.state.newMessageContent} onChange={this.onChange}></textarea>
  //       <div className="buttons">
  //         <button onClick={this.onSave}>Ok</button>
  //         <button onClick={this.onCancel}>Cancel</button>
  //       </div>
  //     </div>
  //   )
  // }


  getModalContent() {
    return (
      <div className="modal">
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Modal title</p>
            <button className="delete" aria-label="close"></button>
          </header>
          <section 
            className="modal-card-body" 
            value={this.state.newMessageContent} 
            onChange={this.onChange}
            >
          </section>
          <footer className="modal-card-foot">
            <button className="button is-success" onClick={this.onSave}>Save changes</button>
            <button className="button" onClick={this.onCancel}>Cancel</button>
          </footer>
        </div>
      </div>
    );
  }

  render() {
    return this.getModalContent();
  }
}

const mapsStateToProps = state => {
  const { currentMessage } = state.modal;
  return {
    currentMessage,
  }
}

const mapDispatchToProps = {
  ...actions,
  updateMessage
}

export default connect(mapsStateToProps, mapDispatchToProps)(MessageEditor);
